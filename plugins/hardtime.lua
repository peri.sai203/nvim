-- Tooltips to force you to use vim movements. Disables mouse and arrow keys
return {
  "m4xshen/hardtime.nvim",
  lazy = true,
  enabled = false,
  event = "VeryLazy",
  opts = {},
}
