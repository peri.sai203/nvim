-- When using LSP rename, will highlight all instances of variable you are
-- renaming
return {
  "smjonas/inc-rename.nvim",
  lazy = false,
  config = function() require("inc_rename").setup() end,
}
