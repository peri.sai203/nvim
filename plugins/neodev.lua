-- Plugin lets you peek at signiture/help documentation of function
-- <leader>lh
return {
  "folke/neodev.nvim",
  lazy = true,
  config = function() require("neodev").setup() end,
}
