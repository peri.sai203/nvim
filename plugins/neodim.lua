return {
  "zbirenbaum/neodim",
  lazy = true,
  event = "LspAttach",
  opts = {
    alpha = 0.1,
    blend_color = "#E67E80",
    update_in_insert = {
      enable = true,
      delay = 100,
    },
    hide = {
      virtual_text = true,
      signs = true,
      underline = true,
    },
  },
}
